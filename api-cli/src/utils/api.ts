import axios, { AxiosError, AxiosInstance, AxiosResponse } from "axios";

let Api: AxiosInstance = axios.create();

export interface ApiResponse<T> {
    data: T;
    error?: AxiosError;
}

export const initAxios = (baseUrl: string, errorHandler: (error: AxiosError) => void) => {
    Api.defaults.baseURL = baseUrl;
    Api.defaults.responseType = 'json';

    Api.interceptors.response.use(
        (response: AxiosResponse) => response,
        (error: AxiosError) => {
            errorHandler && errorHandler(error);
            return Promise.resolve({ error });
        }
    )
}

export default Api;