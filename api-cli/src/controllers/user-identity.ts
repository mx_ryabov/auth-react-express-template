import { IUser } from '../models';
import Api from '../utils/api';

export class UserIdentityApi {
    static login(email:string, password:string) {
        return Api.post<IUser>('/user-identity/login', {email, password});
    }
    
    static logout() {
        return Api.post('/user-identity/logout');
    }

    static register(request:IUser & {password: string}) {
        return Api.post<IUser>('/user-identity/register', request);
    }
}