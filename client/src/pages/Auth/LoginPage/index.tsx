import * as React from 'react';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers';
import { useForm } from 'react-hook-form';
import InputText from '../../../components/InputText';

type FormData = {
    email: string;
    password: string;
};

const schema = yup.object().shape({
    email: yup.string().required('Email is a required field').email('Email must be a valid'),
    password: yup.string().required('Password is a required field'),
});

const LoginPage: React.FC = () => {
    const { register, handleSubmit, errors, reset } = useForm<FormData>({
        resolver: yupResolver(schema),
    });

    const onSubmit = handleSubmit((data) => {
        console.log(data);
        reset();
    });
    return (
        <div>
            <h1>Login</h1>
            <form onSubmit={onSubmit}>
                <InputText name="email" label="Email" ref={register} error={errors.email?.message} />
                <InputText
                    name="password"
                    label="Password"
                    ref={register}
                    type="password"
                    error={errors.password?.message}
                />
                <input type="submit" value="Submit" />
            </form>
        </div>
    );
};

export default LoginPage;
