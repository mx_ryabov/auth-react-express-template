import { NextFunction, Request, Response } from "express";
import passport from "passport";
import "../passportHandler";
import { IBaseUser } from "../interfaces";
import config from "../config";
import * as jwt from "jsonwebtoken";
import BlackList from "../models/BlackList";

export class AuthController {

    public async auth(req:Request, res:Response, next:NextFunction) {
        
        passport.authenticate("local", {session: false}, function (err, user, info) {
            if (err) return next(err);
            if (!user) {
                return res.status(401).json({ status: "error", code: "unauthorized" });
            } else {
                const token = jwt.sign({ login: user.login }, config.session.jwtSecret);
                res.status(200).send({ token: token, user: user });
            }
        })(req, res, next);
    }
    
    public authenticateJWT(req: Request, res:Response, next:NextFunction) {
        passport.authenticate("jwt", async function(err, user:IBaseUser, info) {
            if (err) {
                console.log('Controller authenticateJWT error: ', err);
                return res.status(401).json({ status: "error", code: "unauthorized" });
            }
            if (!req.headers.authorization) {
                res.status(401).json({ status: "error", code: "unauthorized" });
            } else {
                const token:string = req.headers.authorization.split(" ")[1];
                const isInsideBL = await BlackList.findOne({token});
                if (isInsideBL) {
                    res.status(401).json({ status: "error", code: "unauthorized" });
                } else {
                    if (!user) {
                        return res.status(401).json({ status: "error", code: "unauthorized" });
                    } else {
                        req.me = user;
                        return next();
                    }
                }
            }
            
        })(req, res, next);
    }

    public logout(req:Request, res:Response, next:NextFunction) {        
        BlackList.create({token: req.body.token});
        res.json({})
    }
        
}