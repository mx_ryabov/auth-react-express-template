import * as React from 'react';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers';
import { useForm } from 'react-hook-form';
import InputText from '../../../components/InputText';
import { useDispatch } from 'react-redux';
import { RegisterAction } from '../../../store/auth/auth.actions';
import { UserRole } from '@common/api';

type FormData = {
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    confirmPassword: string;
};

const schema = yup.object().shape({
    firstName: yup.string().required('First Name is a required field'),
    lastName: yup.string().required('Last Name is a required field'),
    email: yup.string().required('Email is a required field').email('Email must be a valid'),
    password: yup.string().required('Password is a required field'),
    confirmPassword: yup.string().oneOf([yup.ref('password')], 'Password must match'),
});

const SignUpPage: React.FC = () => {
    const dispatch = useDispatch();
    const { register, handleSubmit, errors, reset } = useForm<FormData>({
        resolver: yupResolver(schema),
    });

    const onSubmit = handleSubmit((data) => {
        dispatch({
            ...new RegisterAction({
                firstName: data.firstName,
                lastName: data.lastName,
                password: data.password,
                email: data.email,
                role: UserRole.Admin,
            }),
        });
        reset();
    });
    return (
        <div>
            <h1>SignUp</h1>
            <form onSubmit={onSubmit}>
                <InputText name="firstName" label="First Name" ref={register} error={errors.firstName?.message} />
                <InputText name="lastName" label="Last Name" ref={register} error={errors.lastName?.message} />
                <InputText name="email" label="Email" ref={register} error={errors.email?.message} />
                <InputText
                    name="password"
                    label="Password"
                    ref={register}
                    type="password"
                    error={errors.password?.message}
                />
                <InputText
                    name="confirmPassword"
                    label="Confirm password"
                    type="password"
                    ref={register}
                    error={errors.confirmPassword?.message}
                />
                <input type="submit" value="Submit" />
            </form>
        </div>
    );
};

export default SignUpPage;
