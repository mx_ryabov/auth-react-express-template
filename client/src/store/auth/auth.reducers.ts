import { IUser, RegisterStatus } from "@common/api";
import { 
    AuthActions, LogInErrorAction, LogInAction, 
    LogInSuccessAction, RegisterAction, RegisterSuccessAction, RegisterErrorAction 
} from "./auth.actions";

export interface AuthState {
    isLoading: boolean;
    userInfo?: IUser;
    authError?: string;
    isAuthorized: boolean;
    isInitialized: boolean;
    registerStatus?: RegisterStatus;
}

const initialState: AuthState = {
    isLoading: false,
    userInfo: undefined,
    authError: undefined,
    isAuthorized: false,
    isInitialized: false,
    registerStatus: undefined
}

export function authReducer(state = initialState, action: AuthActions) {
    switch(action.type) {
        case LogInAction.Name:
            return {
                ...state,
                isLoading: true
            }

        case LogInSuccessAction.Name:
            return {
                isInitialized: true,
                isLoading: false,
                isAuthorized: true,
                authError: undefined,
                userInfo: action.user
            }

        case LogInErrorAction.Name:
            return {
                ...state,
                isLoading: false,
                isInitialized: true,
                authError: action.error
            }

        case RegisterAction.Name:
            return {
                ...initialState,
                isLoading: true
            }

        case RegisterSuccessAction.Name:
            return {
                ...state,
                isLoading: false,
                isInitialized: true,
                registerStatus: action.status
            }

        case RegisterErrorAction.Name:
            return {
                ...state,
                isLoading: false,
                isInitialized: true,
                authError: action.error,
                registerStatus: action.status
            }

        default:
            return state
    }
}