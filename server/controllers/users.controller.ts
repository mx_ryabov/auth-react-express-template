import { Request, Response, NextFunction } from "express";
import User from '../models/User';
import {IBaseUser} from '../interfaces';


export class UsersController {
    public async get_users(req: Request, res: Response, next: NextFunction) {
        var users:IBaseUser[] = await User.find({});
        res.json({me: req.me, users})
    }

    public async create_user(req: Request, res: Response, next: NextFunction) {
        var user:IBaseUser;
        
        try {            
            user = new User(req.body);
            user = await user.save();
            user = user.toObject();
            delete user.hashedPassword;
            delete user.salt;
            res.json({success: true});
        } catch(e) {
            console.log('wtf user controllers: ', e);
            res.status(400).json({success: false, error: e})
        };
    }

    
}