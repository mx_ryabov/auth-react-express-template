import { RegisterStatus, UserIdentityApi } from "@common/api";
import { SagaIterator } from "redux-saga";
import { call, put, takeLatest } from "redux-saga/effects";
import { LogInAction, LogInErrorAction, LogInSuccessAction, RegisterAction, RegisterErrorAction, RegisterSuccessAction } from "./auth.actions";

function* loginHandler(action:LogInAction):SagaIterator {
    const {data, error} = yield call(UserIdentityApi.login, action.email, action.password);

    if (data) {
        yield put({...new LogInSuccessAction(data)});
    } else {
        yield put({...new LogInErrorAction(error)});
    }
}

function* registerHandler(action:RegisterAction) {
    const {error} = yield call(UserIdentityApi.register, action.request);

    if (!error) {
        yield put({...new RegisterSuccessAction(RegisterStatus.Success)});
    } else {
        yield put({...new RegisterErrorAction(RegisterStatus.ServerError, error)})
    }
}

export default function* authSaga():SagaIterator {
    yield takeLatest(LogInAction.Name, loginHandler);
    yield takeLatest(RegisterAction.Name, registerHandler);
}